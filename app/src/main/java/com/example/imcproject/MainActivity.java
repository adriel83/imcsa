package com.example.imcproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button resultButton = findViewById(R.id.botao);

        final EditText nomeText = findViewById(R.id.nome);
        final EditText pesoText = findViewById(R.id.peso);
        final EditText alturaText = findViewById(R.id.altura);
        final EditText idadeText = findViewById(R.id.idade);



        //RadioGroup radioGroup = findViewById(R.id.grupo);
        final RadioButton homemRadio = findViewById(R.id.homem);
       final RadioButton mulherRadio = findViewById(R.id.mulher);



        resultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView resultadoText = findViewById(R.id.resultado);
                Pessoa pessoa = new Pessoa();
                pessoa.setNome(nomeText.getText().toString());
                pessoa.setIdade(Integer.parseInt(idadeText.getText()
                        .toString()));
                pessoa.setAltura(Double.parseDouble(alturaText
                        .getText().toString()));
                pessoa.setPeso(Double.parseDouble(pesoText.getText()
                        .toString()));

                if (homemRadio.isChecked())
                {
                    pessoa.setSexo(Sexo.HOMEM);
                }
                else if(mulherRadio.isChecked())
                {
                    pessoa.setSexo(Sexo.MULHER);
                }

                double imc =
                        pessoa.getPeso()/(pessoa.getAltura()*pessoa.getAltura());

                if ( imc <= 17){
                    //Muito abaixo
                    resultadoText.setText("Abaixo do peso \n IMC: " +imc+ "sexo: "+pessoa.getSexo());
                }else if(imc>17 && imc<20){
                    //Regular
                    resultadoText.setText("Peso normal \n IMC: " +imc + "sexo: "+pessoa.getSexo());
                }else if(imc>=20){
                    //Acima
                    resultadoText.setText("Acima do peso \n IMC: " +imc+ "sexo: "+pessoa.getSexo());
                }
            }
        });


    }
}
